#Readme

install instructions: http://ionicframework.com/docs/guide/installation.html

basically just install Node.js, then install ionic and cordova globally (-g) using npm.

then install the local npm packages (which are listed in package.json) by running `$	npm install` from the project directory (this installs them to the "node_modules" folder which is .gitignored)

then install the local cordova plugins by running `$	ionic state restore`. this installs them to the "plugins" folder.


Building the App:
NOTE: These instructions are required on MacOS/OS X. They will work for Windows and Linux but may have redundant steps for those platforms.

Run the following command once before building for either iOS or Android `$	   ionic hooks add`.

To build for Android, run the following commands:
`$	ionic resources android`
`$	ionic platform add android`
`$	ionic build android`
The built .apk can be found in /oclw-ionic-app/platforms/android/build/outputs/apk

To build for iOS, run the following command:
`$	ionic platform add ios`
Then take the OCLW-Info.plist file found in /oclw-ionic-app/iOS Files and move it to /oclw-ionic-app/platforms/ios/OCLW. This will overwrite the .plist file already in that folder. Now run the following command:
`$	ionic build ios`
Navigate to /oclw-ionic-app/platforms/ios and open OCLW.xcodeproj in Xcode. Change the Bundle Identifier to something unique for your team, and choose your team under "Signing". You should now be able to run the app through the Simulator or on your connected iOS device.