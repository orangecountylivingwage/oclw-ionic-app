// COMP 523 Fall 2016 - Orange County Living Wage
// Mobile App made by Eric Barbee & Max Hudnell

angular.module('oclw')
  .controller('searchCtrl', function ($http, $scope, $stateParams, $cordovaGeolocation, $ionicLoading, $ionicPopover) {

    console.log($stateParams.searchTerm);

    $scope.orgs = [];
    $scope.formData = {};
    $scope.formData.query = decodeURI($stateParams.searchTerm);

    getResults($stateParams.searchTerm);

    function getResults(terms) {
      $scope.noResults = false;
      $scope.showContent = false;

      $ionicLoading.show({
        template: '<ion-spinner class="spinner-light"></ion-spinner>',
        noBackdrop: false
      }).then(function () {
        // $scope.showContent = false;
      });

      $scope.showTimeout = false;
      var posOptions = { timeout: 5000, enableHighAccuracy: false };
      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
          var lat = position.coords.latitude;
          var long = position.coords.longitude;
          console.log(lat + '   ' + long);
          // alert(lat + '   ' + long)

          httpCall(true, terms, lat, long);
          $scope.showDistance = true;
        
        }, function (err) {
          // console.log(err)
          httpCall(false, terms, 0, 0);
        });
    }

    $scope.search = function () {
      var query = $scope.formData.query;

      getResults(query);
    }

    function httpCall(locationEnabled, terms, lat, long) {
      if(locationEnabled) {
        $http({
            method: 'GET',
            url: 'http://oclwapp.azurewebsites.net/api/search?query=' + terms + '&lat=' + lat + '&long=' + long,
            timeout: 10000
          }).then(function successCallback(response) {
            // alert('success');
            console.log(response.data);
            $scope.orgs = response.data.result;
            var count = response.data.count;

            $ionicLoading.hide();
            $scope.showContent = true;

            if(count == 0) {
              $scope.noResults = true;
            }
          }, function errorCallback(response) {
            // alert('fail');
            console.log('GET FAIL!');

            $scope.showContent = false;
            $scope.showTimeout = true;
            $ionicLoading.hide();
          });
      } else {
        $http({
            method: 'GET',
            url: 'http://oclwapp.azurewebsites.net/api/search?query=' + terms,
            timeout: 10000
          }).then(function successCallback(response) {
            // alert('success');
            console.log(response.data);
            $scope.orgs = response.data.result;
            var count = response.data.count;

            $ionicLoading.hide();
            $scope.showContent = true;

            if(count == 0) {
              $scope.noResults = true;
            }
          }, function errorCallback(response) {
            // alert('fail');
            console.log('GET FAIL!');

            $scope.showContent = false;
            $scope.showTimeout = true;
            $ionicLoading.hide();
          });
      }
    }

  });