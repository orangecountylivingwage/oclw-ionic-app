// COMP 523 Fall 2016 - Orange County Living Wage
// Mobile App made by Eric Barbee & Max Hudnell

angular.module('oclw')
  .controller('categoriesCtrl', function($http, $scope, $stateParams, $ionicLoading, $state) {
    // alert('categctrl');
    $scope.cats = [];
    $scope.formData = {};
    $scope.showContent = false;
    
    showSpinner();
    loadPage();

    function showSpinner() {
      $ionicLoading.show({
        template: '<ion-spinner class="spinner-light"></ion-spinner>',
        noBackdrop: false
      }).then(function(){
        // $scope.showContent = false;
      });
    }
    
    function loadPage() {
      $scope.showTimeout = false;
      if (typeof $stateParams.categoryId !== 'undefined') {
        //parentName is used as the nav-bar title
        $scope.parentName = $stateParams.categoryName;

        $http({
          method: 'GET',
          url: 'http://oclwapp.azurewebsites.net/api/categories?ParentId='+$stateParams.categoryId,
          timeout: 10000
        }).then(function successCallback(response) {
          console.log(response.data);
          $scope.cats = response.data;
          for (i = 0; i < $scope.cats.length; i++){   // assign hrefs for each cat in the array
            if ($scope.cats[i].isBottom) {
              $scope.cats[i].hrefUrl = '#/organizations/'+$scope.cats[i].id+'/'+encodeURI($scope.cats[i].CategoryName);
            } else {
              $scope.cats[i].hrefUrl = '#/categories/'+$scope.cats[i].id+'/'+encodeURI($scope.cats[i].CategoryName);
            }
          }

          $ionicLoading.hide();
          $scope.showContent = true;
        }, function errorCallback(response) {
          console.log('GET FAIL!');

          $scope.showContent = false;
          $scope.showTimeout = true;
          $ionicLoading.hide();
        }).finally(function() {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        });
      } else {
        $http({
          method: 'GET',
          url: 'http://oclwapp.azurewebsites.net/api/categories',
          timeout: 10000
        }).then(function successCallback(response) {
          console.log(response.data);
          $scope.cats = response.data;
          for (i = 0; i < $scope.cats.length; i++){   // assign hrefs for each cat in the array
            if ($scope.cats[i].isBottom) {
              $scope.cats[i].hrefUrl = '#/organizations/'+$scope.cats[i].id+'/'+encodeURI($scope.cats[i].CategoryName);
            } else {
              $scope.cats[i].hrefUrl = '#/categories/'+$scope.cats[i].id+'/'+encodeURI($scope.cats[i].CategoryName);
            }
          }

          $ionicLoading.hide();
          $scope.showContent = true;
        }, function errorCallback(response) {
          console.log('GET FAIL!');

          $scope.showContent = false;
          $scope.showTimeout = true;
          $ionicLoading.hide();
        }).finally(function() {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        });
      }
    }

    $scope.search = function() {
      var query = $scope.formData.query;
      console.log(query);
      $state.go('search', {searchTerm: encodeURI(query)});
    };
    
    $scope.doRefresh = function() {
      loadPage();
    };

 });