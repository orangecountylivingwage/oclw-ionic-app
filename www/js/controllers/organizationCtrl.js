// COMP 523 Fall 2016 - Orange County Living Wage
// Mobile App made by Eric Barbee & Max Hudnell

angular.module('oclw')
  .controller('organizationCtrl', function($http, $scope, $stateParams, $ionicLoading) {

    console.log($stateParams.orgId);

    $scope.deets = {};
    $scope.showContent = false;

    //name is used as the nav-bar title
    $scope.name = $stateParams.orgName;

    //Holds the phone number for use in multiple functions.
    var phoneNum;

    $ionicLoading.show({
      template: '<ion-spinner class="spinner-light"></ion-spinner>',
      noBackdrop: false
    }).then(function(){
      // $scope.showContent = false;
    });

    loadPage();

    function loadPage() {
      $scope.showTimeout = false;
      $http({
        method: 'GET',
        url: 'http://oclwapp.azurewebsites.net/api/employer?Id=' +$stateParams.orgId,
        timeout: 10000
      }).then(function successCallback(response) {
        //alert('success');
        console.log(response.data);
        $scope.deets = response.data[0];
        // console.log($scope.deets);

        //If there is a website, show the "Visit Website" button
        if($scope.deets.Website) {  // won't allow null or empty string
          $scope.showWebsite = true;
        }

        //Removes spaces for the "Call Now" functionality and to check for a blank number
        phoneNum = $scope.deets.Phone;
        phoneNum = phoneNum.replace(/\s/g, '');
        
        // Shows or hides "Call Now"
        if(phoneNum) {
          $scope.showPhone = true;
        }
        //Shows or hides "Get Directions"
        if($scope.deets.Lat && $scope.deets.Long) {
          $scope.showDirections = true;
        }

        $ionicLoading.hide();
        $scope.showContent = true;
      }, function errorCallback(response) {
        //alert('failure');
        console.log('GET FAILED');

        $scope.showContent = false;
        $scope.showTimeout = true;
        $ionicLoading.hide();
      }).finally(function() {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      });
    }
    
    $scope.openWebsite = function() {
      var url = $scope.deets.Website;

      //window.open() doesn't work correctly without 'http://', so checking for that
      if(url.includes("http://")) {
        window.open(url, '_system');
      } else {
        window.open('http://'+url, '_system');
      }
    }


    $scope.openDirections = function() {
      var location = $scope.deets.Lat + ',' + $scope.deets.Long;

      if(ionic.Platform.isIOS()) {
        appAvailability.check(
          'comgooglemaps://',
          function() { //Google Maps is installed
            window.open('comgooglemaps://?daddr='+location, '_system');
          },
          function() { //Use Apple Maps
            window.open('maps:?daddr='+location, '_system');
          }
        );
        

      } else if(ionic.Platform.isAndroid()) {
      //This doesn't work anymore, Google changed how parameters are sent to Maps
      //window.open('geo:?daddr='+location, '_system');

        window.open('http://google.com/maps/?daddr='+location, '_system');  

      } else {
        //alert('couldn't detect OS');
        console.log('Could not detect mobile OS');

        window.open('http://google.com/maps/?daddr='+location, '_system');
      }
      
    }

    $scope.callNumber = function() {
      window.open('tel:'+phoneNum, '_system');
    }

    $scope.doRefresh = function() {
      loadPage();
    };
 });