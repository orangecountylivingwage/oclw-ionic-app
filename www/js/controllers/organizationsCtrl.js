// COMP 523 Fall 2016 - Orange County Living Wage
// Mobile App made by Eric Barbee & Max Hudnell

angular.module('oclw')
  .controller('organizationsCtrl', function($http, $scope, $stateParams, $cordovaGeolocation, $ionicLoading, $ionicPopover) {

    console.log($stateParams.categoryId);
    // alert($stateParams.categoryId);

    $scope.orgs = [];
    $scope.showContent = false;
    $scope.sortedBy = '';

    //parentName is used as the nav-bar title
    $scope.parentName = $stateParams.categoryName;
    
    //Checks if this is the Featured category
    var featured = false;
    if($scope.parentName.includes("Featured")) {
      featured = true;
    }
    
    

    $ionicLoading.show({
      template: '<ion-spinner class="spinner-light"></ion-spinner>',
      noBackdrop: false
    }).then(function(){
      // $scope.showContent = false;
    });

    loadPage();

    function loadPage() {
      $scope.showTimeout = false;
      var posOptions = {timeout: 5000, enableHighAccuracy: false};
      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
          var lat  = position.coords.latitude;
          var long = position.coords.longitude;
          console.log(lat + '   ' + long);
          // alert(lat + '   ' + long)

          httpCall(true, lat, long);
          $scope.showDistance = true;

        }, function (err) {
          // console.log(err)
          httpCall(false, 0, 0);

        }).finally(function() {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        });
    }

    $ionicPopover.fromTemplateUrl('templates/sort-popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });

    $scope.doRefresh = function() {
      loadPage();
    };

    $scope.sort_alpha = function() {
      $scope.orgs.sort(sortByProp('EmployerName'));
      $scope.sortedBy = 'name';
      $scope.popover.hide();
    };

    $scope.sort_dist = function() {
      $scope.orgs.sort(sortByProp('distance'));
      $scope.sortedBy = 'dist';
      $scope.popover.hide();
    };

    function sortByProp(property) {
      return function (a,b) {
          return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      }
    }

    function httpCall(locationEnabled, lat, long) {
      if(locationEnabled) {
        $http({
            method: 'GET',
            url: 'http://oclwapp.azurewebsites.net/api/employers?CategoryId=' + $stateParams.categoryId + '&lat=' + lat + '&long=' + long,
            timeout: 10000
          }).then(function successCallback(response) {
            // alert('success');
            console.log(response.data);
            $scope.orgs = response.data;
            $scope.sortedBy = 'dist';

            //If no businesses are featured, show a message
            if(featured && response.data.length == 0) {
              $scope.noResults = true;
            }

            $ionicLoading.hide();
            $scope.showContent = true;
          }, function errorCallback(response) {
            // alert('fail');
            console.log('GET FAIL!');
            $scope.sortedBy = '';

            $scope.showContent = false;
            $scope.showTimeout = true;
            $ionicLoading.hide();
          });
      } else {
        $http({
            method: 'GET',
            url: 'http://oclwapp.azurewebsites.net/api/employers?CategoryId=' + $stateParams.categoryId,
            timeout: 10000
          }).then(function successCallback(response) {
            // alert('success');
            console.log(response.data);
            $scope.orgs = response.data;
            $scope.sortedBy = 'dist';

            $ionicLoading.hide();
            $scope.showContent = true;
          }, function errorCallback(response) {
            // alert('fail');
            console.log('GET FAIL!');
            $scope.sortedBy = '';

            $scope.showContent = false;
            $scope.showTimeout = true;
            $ionicLoading.hide();
          });
      }
    }

});